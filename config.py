import os

WTF_CSRF_ENABLED = True
SECRET_KEY = '56654894261543124465sad465as421'
SECURITY_PASSWORD_SALT = '213123542342343asdfsad65'

basedir = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'cw2.db')
SQLALCHEMY_TRACK_MODIFICATIONS = True
UPLOAD_FOLDER = 'D:/uploads'
