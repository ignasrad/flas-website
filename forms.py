from flask_wtf import Form
from flask_wtf.file import FileField, FileRequired
from wtforms import TextField, PasswordField, IntegerField, SelectField, TextAreaField
from wtforms.validators import DataRequired,EqualTo, Email, Length

class RegisterForm(Form):
    email = TextField('Email', validators=[DataRequired('Please enter a valid email address.'),
                                             Email('Please enter a valid email address.')])
    password = PasswordField('New Password', [DataRequired(), EqualTo('confirm', message='Passwords must match'), Length(max=20)])
    confirm  = PasswordField('Repeat Password')

class LoginForm(Form):
    email = TextField('Email', validators=[DataRequired('Please enter a valid email address.'),
                                             Email('Please enter a valid email address.')])
    password = PasswordField('Password', validators = [DataRequired()])

class ChangeForm(Form):
    password = PasswordField('New Password', [DataRequired(), EqualTo('confirm', message='Passwords must match'), Length(max=20)])
    confirm  = PasswordField('Repeat New Password')

class ProfileInfo(Form):
    photo = FileField()
    name = TextField('Name', validators=[DataRequired('Please enter a valid name'), Length(max=20)])
    surname = TextField('Last Name', validators=[DataRequired('Please enter a valid surname'), Length(max=20)])
    age = IntegerField('Age', validators=[DataRequired('Please enter age as an integer')])
    team = TextField('Current team', validators=[Length(max=20)])
    availible = TextField('Availible',validators=[Length(max=20)])
    position = SelectField(u'Programming Language',choices=[('PG', 'Point Guard'),
                                                            ('SG', 'Shooting Guard'),
                                                            ('PF', 'Power Forward'),
                                                            ('SF', 'Small Forward'),
                                                            ('C', 'Center')])
class CommentForm(Form):
    text = TextAreaField('Text', render_kw={"rows": 6, "cols": 80},validators=[Length(max=300)])
