from cw2 import app, models, db
from flask import render_template,redirect,request,flash, session
from .forms import ChangeForm, RegisterForm, ProfileInfo, CommentForm
from flask_security import SQLAlchemyUserDatastore, Security,logout_user, login_required, login_user, current_user
from flask_security.utils import hash_password, verify_and_update_password
from werkzeug.utils import secure_filename
from flask_images import resized_img_src
import os
import json
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s:   %(asctime)s:%(name)s:%(message)s')
file_handler = logging.FileHandler('views.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
security = Security(app, user_datastore)

@app.route('/')
@login_required
def index():
    form = CommentForm()
    profiles = models.User.query.order_by(models.User.endorsements.desc())
    if(not current_user.name or not current_user.surname or not current_user.position):
        logger.warning('User: ' + str(current_user.id) + ' Redericted to edit page (for addding more info)')
        flash("Before continuing please fill in some essential information (name, surname, position)")
        return redirect('/edit')
    return render_template('index.html', title = 'BasketIn', profiles = profiles, form = form)

@app.route('/saved')
@login_required
def saved():
    form = CommentForm()
    profiles = current_user.saved
    if(not current_user.name or not current_user.surname or not current_user.position):
        flash("Before continuing please fill in some essential information (name, surname, position)")
        logger.warning('User: ' + str(current_user.id) + ' Redericted to edit page (for addding more info)')
        return redirect('/edit')
    logger.info('Saved_page generated successfully user id: ' + str(current_user.id))
    return render_template('index.html', title = 'BasketIn', profiles = profiles, form = form)

@app.route('/register', methods=['POST', 'GET'])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        em = form.email.data
        user = models.User.query.filter_by(email = em).first()
        if (not user):
            user_datastore.create_user(
                email = em,
                password = hash_password(form.password.data)
            )
            db.session.commit()
            logger.info('New account created email: ' + em)
            return redirect('/')
        else:
            logger.error('Trying to register account with existing email')
            form.email.errors.append("There already is an account with this email")
    return render_template('register.html', form = form, title = "Sign up")

@app.route("/password", methods=['POST', 'GET'])
@login_required
def password():
    form = ChangeForm()
    psw = form.password.data
    msg = ''
    if (form.validate_on_submit()):
        current_user.password = hash_password(psw)
        db.session.commit()
        msg = "Password was changed succesfully!"
        logger.info("Password was changed succesfully! user id: " + str(current_user.id))

    return render_template('settings.html', title = 'Change password', profile = 0, form = form, msg = msg)

@app.route("/profile")
@login_required
def profile():

    profile = [current_user.name, current_user.surname, current_user.age, current_user.position,
    current_user.team, current_user.availible]
    logger.info("Profile info page created! user id: " + str(current_user.id))
    return render_template('settings.html', title = 'Profile', profile = profile, current = current_user)

@app.route("/edit", methods=['POST', 'GET'])
@login_required
def edit():
    form = ProfileInfo()
    profile = [current_user.name, current_user.surname, current_user.age, current_user.position]
    if (form.validate_on_submit()):
        f = form.photo.data
        if(f):
            filename = secure_filename(f.filename)
            f.save(os.path.join(
                app.root_path,'static/photos', str(current_user.id)
            ))
            if(os.path.exists(os.path.join(
                app.root_path,'static/photos', str(current_user.id)
            ))):
                current_user.img = True
        else:
            logger.error("Selected file does not exist!!!! User id: " + str(current_user.id))

        current_user.name = form.name.data
        current_user.surname = form.surname.data
        current_user.age = form.age.data
        current_user.position = form.position.data
        current_user.team = form.team.data
        current_user.availible = form.availible.data
        db.session.commit()
        logger.info("Profile info edited for user id: " + str(current_user.id))
        return redirect('/profile')
    logger.info("Profile edit page created! user id: " + str(current_user.id))
    return render_template('profile.html', title = 'Edit profile',form = form)

@app.route("/delete")
@login_required
def delete():
    current_user.img = False
    db.session.commit()
    logger.info("Img was deleted user id: " + str(current_user.id))
    return redirect("/profile")

@app.route("/logout")
@login_required
def logout():
    session['voted'] = False
    # """Logout the current user."""
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    logger.info("Loged out, user id: " + str(current_user.id))
    return index()


@app.route("/save", methods=['POST'])
@login_required
def save():
    # Parse the JSON data included in the request
    rule = request.path
    print(rule)
    data = json.loads(request.data)
    id = data.get('id')
    toSave = models.User.query.get(id)
    current_user.save(toSave)
    db.session.commit()
    logger.info("User id: " + str(current_user.id) + " Saved: " + str(id))
    return json.dumps({'status': 'ok', 'response': id})

@app.route("/like", methods=['POST'])
@login_required
def like():
    data = json.loads(request.data)
    id = data.get('id')
    session["voted"] = True
    print(session["voted"])
    profile = models.User.query.get(id)
    profile.endorsements += 1
    db.session.commit()
    logger.info("User id: " + str(current_user.id) + " endorsed: " + str(id))
    session['vote'] = profile.name + ' ' + profile.surname
    return json.dumps({'status': 'ok', 'id': id, 'votes': profile.endorsements})


@app.route("/comment", methods=['POST'])
@login_required
def comment():
    data = json.loads(request.data)
    id = data.get('id')
    text = data.get('text')
    comment = models.Comment(comment=text, nameFrom = current_user.name + ' ' + current_user.surname, on_id=id)
    profile = models.User.query.get(id)
    profile.comment(comment);
    db.session.commit()
    logger.info("User id: " + str(current_user.id) + " Commented: " + str(id))
    session['comment'] = profile.name + ' ' + profile.surname
    return json.dumps({'status': 'ok', 'id': id, 'name': current_user.name + ' ' + current_user.surname})
