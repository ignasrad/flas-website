import os, sys
import unittest

from cw2 import models, db

class Test_Default_Values(unittest.TestCase):
    def test_default_saved(self):
        user1 = models.User()
        assert len(user1.saved) ==  0

    def test_default_comments(self):
        user1 = models.User()
        assert len(user1.comments) ==  0

class Test_Saving_Users(unittest.TestCase):

    def test_saves_another_user(self):
        user1 = models.User()
        user2 = models.User()
        user1.save(user2)
        assert user1.saved[0] == user2

    def test_does_not_eddit_added_user(self):
        user1 = models.User()
        user2 = models.User()
        user1.save(user2)
        assert len(user2.saved) ==  0

    def test_unsaves_user(self):
        user1 = models.User()
        user2 = models.User()
        user1.save(user2)
        user1.save(user2)
        assert len(user1.saved) ==  0


class Test_Adding_Comments(unittest.TestCase):

    def test_adds_comment(self):
        user1 = models.User()
        comment = models.Comment()
        user1.comment(comment)
        assert user1.comments[0] == comment

    def test_does_not_add_same_comment(self):
        user1 = models.User()
        comment = models.Comment()
        user1.comment(comment)
        user1.comment(comment)
        assert len(user1.comments) == 1


if __name__ == '__main__':
    unittest.main()
