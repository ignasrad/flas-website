from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_images import Images


app = Flask(__name__)
app.config.from_pyfile('config.py')
db = SQLAlchemy(app)
images = Images(app)

migrate = Migrate(app, db)
admin = Admin(app,template_mode='bootstrap3')

from cw2 import views, models
admin.add_view(ModelView(models.User, db.session))
admin.add_view(ModelView(models.Comment, db.session))
