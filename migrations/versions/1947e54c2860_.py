"""empty message

Revision ID: 1947e54c2860
Revises: 
Create Date: 2019-12-16 17:25:31.457506

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1947e54c2860'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('role',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=40), nullable=True),
    sa.Column('description', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('saved',
    sa.Column('saverId', sa.Integer(), nullable=True),
    sa.Column('savedId', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('savedId')
    )
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('email', sa.String(length=30), nullable=True),
    sa.Column('password', sa.String(length=225), nullable=True),
    sa.Column('active', sa.Boolean(), nullable=True),
    sa.Column('name', sa.String(length=20), nullable=True),
    sa.Column('surname', sa.String(length=20), nullable=True),
    sa.Column('age', sa.Integer(), nullable=True),
    sa.Column('position', sa.String(length=20), nullable=True),
    sa.Column('team', sa.String(length=20), nullable=True),
    sa.Column('availible', sa.String(length=20), nullable=True),
    sa.Column('img', sa.Boolean(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_user_email'), 'user', ['email'], unique=True)
    op.create_table('roles_users',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('roles_users')
    op.drop_index(op.f('ix_user_email'), table_name='user')
    op.drop_table('user')
    op.drop_table('saved')
    op.drop_table('role')
    # ### end Alembic commands ###
