from cw2 import db
from flask_security import UserMixin, RoleMixin
import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
formatter = logging.Formatter('%(levelname)s:   %(asctime)s:%(name)s:%(message)s')
file_handler = logging.FileHandler('database.log')
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)


# creating a table directly for registering
# just like advised in flask_security documentation
roles_users = db.Table('roles_users',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('role_id', db.Integer, db.ForeignKey('role.id'))
)

saved = db.Table(
    'saved', db.Model.metadata,
    db.Column('user_id',db. Integer,db.ForeignKey('user.id'), index=True),
    db.Column('friend_id', db.Integer, db.ForeignKey('user.id')),
    db.UniqueConstraint('user_id', 'friend_id', name='unique_friendships'))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(30), index=True, unique=True)
    password = db.Column(db.String(225))
    active = db.Column(db.Boolean)
    name = db.Column(db.String(20), default='')
    surname = db.Column(db.String(20), default='')
    age = db.Column(db.Integer)
    position = db.Column(db.String(20), default='')
    team = db.Column(db.String(20), default='')
    availible = db.Column(db.String(20), default='')
    img = db.Column(db.Boolean)
    endorsements = db.Column(db.Integer, default=0)
    saved = db.relationship('User',
                           secondary=saved,
                           primaryjoin=id==saved.c.user_id,
                           secondaryjoin=id==saved.c.friend_id,
                           order_by='User.endorsements')
    comments = db.relationship("Comment")
    roles = db.relationship(
            'Role',
            secondary=roles_users,
            backref = db.backref('users', lazy='dynamic'))

    def save(self, friend):
        if friend not in self.saved:
            self.saved.append(friend)
            logger.info('Added new saved account id'+ str(friend.id)  +  'to user_id: ' + str(self.id))
        elif friend in self.saved:
            self.saved.remove(friend)
            logger.info('Deleted saved account id'+ str(friend.id)  +  'from user_id: ' + str(self.id))

    def comment(self, com):
        if com not in self.comments:
            self.comments.append(com)
            logger.info('Added new comment to user_id: ' + str(self.id))

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(40))
    description = db.Column(db.String(255))

class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    comment = db.Column(db.String(300))
    nameFrom = db.Column(db.String(80))
    on_id = db.Column(db.Integer, db.ForeignKey('user.id'))
